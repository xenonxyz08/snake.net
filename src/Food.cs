using SFML.Graphics;
using SFML.System;
using System;

namespace SnakeNET
{
    class Food
    {
        public static RectangleShape NewFood = null;
        public Food()
        {
            Random RNG = new Random();
            int X = RNG.Next(0, (int)Program.WindowSize / Program.SquareSize) * 30;
            int Y = RNG.Next(0, (int)Program.WindowSize / Program.SquareSize) * 30;
            NewFood = new RectangleShape(new Vector2f(Program.SquareSize, Program.SquareSize));
            NewFood.FillColor = Color.Red;
            NewFood.Position = new Vector2f(X, Y);
        }
    }
}