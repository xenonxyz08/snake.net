﻿using System;
using SFML.Graphics;
using SFML.Window;
using SFML.System;
using System.Runtime.InteropServices;

namespace SnakeNET
{
    class Program
    {
        public static uint WindowSize = 540;
        public static int SquareSize = 30;
        public static int Status = 0; // 0 == not started; 1 == started; 2 == ended
        public static RenderWindow Window = new RenderWindow(new VideoMode(WindowSize, WindowSize), "Snake");
        static Snake snake = new Snake();
        static void OnClose(object Sender, EventArgs Event)
        {
            RenderWindow Window = (RenderWindow)Sender;
            Window.Close();
        }
        static void OnKeyInput(object Sender, KeyEventArgs Event)
        {
            snake.ChangeDirection(Event.Code);
            if (Status == 0)
            {
                Status = 1;
            }
        }
        static void Main(string[] args)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                [DllImport("kernel32.dll")]
                static extern IntPtr GetConsoleWindow();

                [DllImport("user32.dll")]
                static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

                const int SW_HIDE = 0;

                var handle = GetConsoleWindow();

                ShowWindow(handle, SW_HIDE);
            }

            Window.Closed += OnClose;
            Window.KeyReleased += OnKeyInput;
            while (Window.IsOpen == true)
            {
                if (Status != 2)
                {
                    snake.Move();
                    Window.DispatchEvents();
                    Window.Clear();
                    snake.RenderSnake();
                    if (Food.NewFood == null)
                    {
                        new Food();
                    }
                    Window.Draw(Food.NewFood);
                    Window.Display();
                }
                else
                {
                    Window.DispatchEvents();
                    Window.Clear();
                    Window.Draw(snake.EndingText);
                    Window.Display();
                }
            }
        }
    }
}
