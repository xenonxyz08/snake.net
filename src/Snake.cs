using System.Collections.Generic;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;

namespace SnakeNET
{
    class Snake
    {
        public Text EndingText = null;
        int Score = 0;
        List<SnakeSegment> Body = new List<SnakeSegment>();
        Clock LastMoved = new Clock();
        Keyboard.Key CurrentDirection = Keyboard.Key.Unknown;
        public Snake()
        {
            Body.Add(new SnakeSegment());
            Body.Add(new SnakeSegment());
            Body.Add(new SnakeSegment());
            Body[0].Position = new Vector2f(Program.WindowSize / 2 - 30, Program.WindowSize / 2 - 30);
            Body[1].Position = Body[0].Position + new Vector2f(0, Program.SquareSize);
            Body[2].Position = Body[1].Position + new Vector2f(0, Program.SquareSize);
        }

        void CheckCollision()
        {
            if (Food.NewFood != null && Food.NewFood.Position == Body[0].Position)
            {
                Body.Add(new SnakeSegment());
                Food.NewFood = null;
                Score++;
                return;
            }
            else if (Body[0].Position.X > Program.WindowSize || Body[0].Position.Y > Program.WindowSize || Body[0].Position.X < 0 || Body[0].Position.Y < 0)
            {
                if (Program.Status == 1)
                {
                    EndingText = new Text();
                    EndingText.Font = new Font("OpenSans.ttf");
                    EndingText.DisplayedString = $"You died! Your score was {Score}.";
                    EndingText.Position = new Vector2f(0, 0);

                    Program.Status = 2;
                    return;
                }
            }
            SnakeSegment Head = Body[0];
            for (int i = 0; i < Body.Count; i++)
            {
                SnakeSegment CurrentSegment = Body[i];
                if (Head.Position == CurrentSegment.Position && i > 0)
                {
                    EndingText = new Text();
                    EndingText.Font = new Font("OpenSans.ttf");
                    EndingText.DisplayedString = $"You died! Your score was {Score}.";
                    EndingText.Position = new Vector2f(0, 0);


                    Program.Status = 2;
                }
            }

        }
        public void ChangeDirection(Keyboard.Key KeyCode)
        {
            if (KeyCode == Keyboard.Key.W && CurrentDirection != Keyboard.Key.S)
            {
                CurrentDirection = KeyCode;
            }
            else if (KeyCode == Keyboard.Key.A && CurrentDirection != Keyboard.Key.D)
            {
                CurrentDirection = KeyCode;
            }
            else if (KeyCode == Keyboard.Key.S && CurrentDirection != Keyboard.Key.W)
            {
                CurrentDirection = KeyCode;
            }
            else if (KeyCode == Keyboard.Key.D && CurrentDirection != Keyboard.Key.A)
            {
                CurrentDirection = KeyCode;
            }
        }
        public void Move()
        {
            if (LastMoved.ElapsedTime.AsMilliseconds() > 125 && Program.Status == 1)
            {
                for (int i = 0; i < Body.Count; i++)
                {
                    SnakeSegment CurrentShape = Body[i];
                    if (i == 0)
                    {
                        CurrentShape.PreviousPosition = CurrentShape.Position;
                        if (CurrentDirection == Keyboard.Key.W)
                        {
                            CurrentShape.Position += new Vector2f(0, -Program.SquareSize);
                        }
                        else if (CurrentDirection == Keyboard.Key.A)
                        {
                            CurrentShape.Position += new Vector2f(-Program.SquareSize, 0);
                        }
                        else if (CurrentDirection == Keyboard.Key.S)
                        {
                            CurrentShape.Position += new Vector2f(0, Program.SquareSize);
                        }
                        else if (CurrentDirection == Keyboard.Key.D)
                        {
                            CurrentShape.Position += new Vector2f(Program.SquareSize, 0);
                        }
                    }
                    else
                    {
                        CurrentShape.PreviousPosition = CurrentShape.Position;
                        CurrentShape.Position = Body[i - 1].PreviousPosition;
                    }
                }
                LastMoved.Restart();
                CheckCollision();
            }
        }
        public void RenderSnake()
        {
            for (int i = 0; i < Body.Count; i++)
            {
                Program.Window.Draw(Body[i]);
            }
        }
    }
}