using SFML.Graphics;
using SFML.System;

namespace SnakeNET
{
    class SnakeSegment : RectangleShape
    {
        public Vector2f PreviousPosition = new Vector2f(0, 0);

        public SnakeSegment()
        {
            base.Size = new Vector2f(Program.SquareSize, Program.SquareSize);
            base.FillColor = Color.Green;
        }
    }
}